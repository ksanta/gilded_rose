<?php

namespace App\Http\Controllers;

use App\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Product::all();
        return view('shop')->with('products', $products);
    }

    public function purchase($id)
    {
        $product = Product::find($id);
        if ($product->quantity > 0) {
            $product->quantity = $product->quantity - 1;
            $product->save();
        }

        return view('purchase')->with('product', $product);
    }
}