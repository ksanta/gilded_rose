<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(array(
            array(
                'name' => 'Animal 1',
                'price' => 20.99,
                'picture_url' => 'https://placebear.com/200/300',
                'purchased' => 0,
                'quantity' => 3,
            ),
            array(
                'name' => 'Animal 2',
                'price' => 40.99,
                'picture_url' => 'https://placebear.com/200/300',
                'purchased' => 0,
                'quantity' => 3,
            ),
            array(
                'name' => 'Animal 3',
                'price' => 60.99,
                'picture_url' => 'https://placebear.com/200/300',
                'purchased' => 0,
                'quantity' => 3,
            ),
        ));
    }
}