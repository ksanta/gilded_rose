@extends('layouts.app')

@section('content')
<div class="container">
    <div class="alert alert-success">
        @if($product->quantity > 0)
        <strong>Thank-You! You Have Just Purchased the {{$product->name}}</strong>
    </div>
    @else
    <div class="text-danger">
        <p>Sorry we are sold out of this item.</p>
    </div>
    @endif
    <div class="row justify-content-center">
        <div class="card" style="width:300px">
            <img class="card-img-top" src="{{$product->picture_url}}" alt="Product image">
            <div class="card-body">
                <h4 class="card-title">{{$product->name}}</h4>
                <p class="card-text">Price: ${{$product->price}}</p>
                <p class="card-text">Quantity: {{$product->quantity}}</p>
                <a href="{{ url('/shop') }}" class="btn btn-primary">Continue Shopping?</a>
            </div>
        </div>
    </div>
</div>
@endsection