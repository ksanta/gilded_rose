@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if(count($products))
        @foreach($products as $product)
        <div class="col">
            <div class="card" style="width:300px">
                <img class="card-img-top" src="{{$product->picture_url}}" alt="Product image">
                <div class="card-body">
                    <h4 class="card-title">{{$product->name}}</h4>
                    <p class="card-text">Price: ${{$product->price}}</p>
                    <p class="card-text">Quantity: {{$product->quantity}}</p>
                    @if($product->quantity > 0)
                    <a href="http://localhost/gilded_rose/public/shop/{{$product->id}}"
                        class="btn btn-primary">Purchase</a>
                    @else
                    <p class="text-danger">Sorry we are currently sold out!</p>
                    @endif
                </div>

            </div>
        </div>
        @endforeach
        @endif
    </div>
    @endsection